var gulp = require('gulp'),
  path = require("path"),
  rename = require('gulp-rename'),
  uglify = require('gulp-uglify'),
  webpack = require('webpack'),
  webpackStream = require('webpack-stream'),
  gulpsync = require('gulp-sync')(gulp),
  browserSync = require('browser-sync').create(),
  reload      = browserSync.reload,
  notify = require("gulp-notify"),

  stylus = require('gulp-stylus'),
  rupture = require('rupture'),

  postcss = require('gulp-postcss'),
  cssMqpacker = require('css-mqpacker'),
  autoprefixer = require('autoprefixer'),
  clipPathPolyfill = require('postcss-clip-path-polyfill'),
  assets  = require('postcss-assets'),
  cssvariables = require('postcss-css-variables'),

  // image
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  jpegtran = require('imagemin-jpegtran'),
  gifsicle = require('imagemin-gifsicle'),
  mozjpeg = require('imagemin-mozjpeg'),
  svgstore = require('gulp-svgstore'),
  svgmin = require('gulp-svgmin')

  cheerio = require('gulp-cheerio'),
  replace = require('gulp-replace');


const paths = {
    public: { 
        js: 'public/js/',
        css: 'public/css/',
        html: 'public/',
        img: 'public/img/',
        fonts: 'public/fonts/',
        svg: 'public/svg/'
    },
    src: { 
        js: 'src/js/',
        html: 'src/',
        styl: 'src/styl/',
        img: 'src/img/',
        fonts: 'src/fonts/',
        svg: 'src/svg/'
    },
    watch: { 
        js: ['src/**/*.js', 'src/**/*.vue'],
        html: ['src/*.*', 'src/include/*.*'],
        styl: 'src/styl/**/*.styl',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/*.*',
        svg: 'src/svg/*.svg'
    },
    clean: './public',
    root: 'public'
};


// Ловим ошибки
const logError = function(err) {
  gutil.log(err);
  this.emit('end');
};

gulp.task('connect', function () {
   gulp.watch( paths.watch.html, ['html'] )
   gulp.watch( paths.watch.styl, ['styl'])
   gulp.watch( paths.watch.js, ['js'] )
   gulp.watch( paths.watch.img, ['img'] ),
   gulp.watch( paths.watch.svg, ['svg'] )
   gulp.watch( paths.watch.fonts, ['fonts'] )
   browserSync.init({
      server: {
            baseDir: paths.root
        },
      port: 3000,
      open: false,
      notify: false
   });
});

gulp.task('default', gulpsync.sync(['html', 'img', 'svg', 'fonts', 'styl', 'js',  'connect']))
  

gulp.task('js', function () {
  // return gulp.src('./src/js/main.js')
  //   .pipe(webpackStream({
  //     entry: {
  //       main: [
  //           path.join(__dirname, 'src/js', 'main.js')
  //       ]
  //   },
  //   output: {
  //       path: path.join(__dirname, '/js/'),
  //       filename: '[name].js',
  //       publicPath: '/js/'
  //   },
  //     module: {
  //       rules: [
  //         {
  //           test: /\.(js)$/,
  //           exclude: /(node_modules)/,
  //           loader: 'babel-loader',
  //           query: {
  //             presets: ['env']
  //           }
  //         },
  //       ]
  //     },

  //     resolve: {
  //       extensions: ['.js', '.jsx','.css'],//add '.css' "root": __dirname
  //       modules: [
  //           'node_modules',
  //           'bower_components',
  //           'bower_components/wow/dist',
  //           'bower_components/slick-carousel/slick',
  //           'bower_components/parallax/deploy',
  //           'src/blocks',
  //           'src/libs/modal',
  //           'src/libs/tablist',
  //           'src/libs/awesomplete',
  //           'src/js',
  //           'src/js/plugins',
  //       ],
  //       alias: {
  //           "TweenLite": path.resolve('node_modules', 'gsap/src/uncompressed/TweenLite.js'),
  //           "TweenMax": path.resolve('node_modules', 'gsap/src/uncompressed/TweenMax.js'),
  //           "TimelineLite": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineLite.js'),
  //           "TimelineMax": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineMax.js'),
  //           "ScrollMagic": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
  //           "animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
  //           "debug.addIndicators": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js'),
  //           "slick": path.resolve('node_modules', 'slick-carousel/slick/slick.js'),
  //           "enquire": path.resolve('node_modules', 'enquire/dist/enquire.js'),
  //           vue: 'vue/dist/vue.js'
  //       },
  //   },
      
  //   }))
  //   .on('error', function handleError() {
  //      this.emit('end'); // Recover from errors
  //   })
  //   //.pipe(uglify())
  //   .pipe(gulp.dest(paths.public.js))
  //   //.pipe(notify({message: "This is a message."}))
  //   //.pipe(rename({ suffix: '.min' }))
  //   //.pipe(gulp.dest('./public/'));
});




let posthtml = require('gulp-posthtml')
const plugins = [ 
  require('posthtml-include')({ root: `${paths.src.html}` }),
  require('posthtml-each')() 
  ]
const options = {}
gulp.task('html', function () {
    gulp.src([paths.src.html+'*.*', paths.src.html+'include/*.*'])
    .pipe(posthtml(plugins, options))
    .pipe(gulp.dest(paths.public.html))
    .pipe(reload({stream:true}));
});

function onError(err) {
  console.log(err);
  this.emit('end');
}


gulp.task('styl', function () {
  gulp.src(paths.src.styl+'*.styl')
  .pipe(stylus({
     'include css': true,
     use: [rupture()],
  }))
  .on('error', onError)
  .pipe(postcss([
    autoprefixer(),
    clipPathPolyfill(),
    // require('postcss-focus-hover')(),
    // cssvariables({
    //   preserve: true,
    // }),
    cssMqpacker({sort: true}),
    require('postcss-easing-gradients')(),
    assets({
      loadPaths: ['src/img/'],
      relative: 'src/stylus'
    }),
    // require('postcss-cssnext')(),
    //require('cssnano')({zindex: false}),
  ]))
  .on('error', onError)
  .pipe(gulp.dest(paths.public.css))
  .pipe(reload({stream:true}));
})


gulp.task('img', () => {
    return gulp.src(paths.src.img+'**/*.*')
        .pipe(imagemin([jpegtran({progressive: true}), mozjpeg({progressive: true}), gifsicle()]
        ))
        .pipe(gulp.dest(paths.public.img));
});

gulp.task('svg', function () {
    return gulp
        .src(paths.src.svg+'**/*.svg')
        .pipe(svgmin(function (file) {
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: 'icon-',
                        minify: true
                    }
                }]
            }
        }))
        // remove all fill, style and stroke declarations in out shapes
        .pipe(cheerio({
          run: function ($) {
            $('svg:not(.prevent-clear) [fill]:not([fill="currentColor"]):not([fill="none"])').removeAttr('fill');
            $('svg:not(.prevent-clear) [stroke]').removeAttr('stroke');
            $('svg:not(.prevent-clear) [style]').removeAttr('style');
            $('svg:not(.prevent-clear) title').remove()
            $('svg.prevent-clear').removeClass('prevent-clear')
          },
          parserOptions: {xmlMode: true}
        }))
        // cheerio plugin create unnecessary string '&gt;', so replace it.
        .pipe(replace('&gt;', '>'))
        .pipe(svgstore())
        .pipe(gulp.dest(paths.public.svg));
});


gulp.task('fonts', function() {
    return gulp.src(paths.src.fonts+'**/*.*')
    .pipe(gulp.dest(paths.public.fonts))
    .pipe(reload({stream:true}));
});