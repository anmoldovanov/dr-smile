$(function() {

    $(document).on('click', 'a[href^="#"]:not(.home-advantages-block__title)', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});

      // instanciate new modal
      var modal = new tingle.modal({
          footer: false,
          stickyFooter: false,
          closeMethods: ['overlay', 'escape'],
          closeLabel: "Close",
          cssClass: ['feedback-modal'],
          onOpen: function() {
              $(this.modal).find('.feedback-modal__close').on('click', function(e) {
                this.close()
              }.bind(this))
          },
          onClose: function() {
              //console.log('modal closed');
          },
      });
      $('body').on('click', '[data-open-feedback-modal]', function(e) {
          modal.open();
      })
      modal.setContent(`
          <button class="btn-base feedback-modal__close"></button>
          <div class="feedback-modal__title">Заказ звонка</div>
          <div class="feedback-modal__desc">
            Мы перезвоним вам сейчас<br />
            или в удобное для звонка время
          </div>
          <form action="" class="feedback-modal__form">
              <label class="field-wrapper">
                 <div class="field-wrapper__title">Ваше имя</div>
                 <input required type="text" name="" class="field-input">
              </label>
              <label class="field-wrapper">
                 <div class="field-wrapper__title">Номер контактного телефона</div>
                 <input required type="text" name="" class="field-input">
              </label>
              <div class="switcher">
                  <div class="switcher__title"></div>
                  <div class="switcher__items">
                      <label class="switcher-label">
                          <input type="radio" class="sr-only" checked name="time" value="Сейчас" />
                          <span class="switcher-label__content">Сейчас</span>
                      </label>
                      <label class="switcher-label">
                          <input type="radio" class="sr-only" name="time" value="По времени" />
                          <span class="switcher-label__content">По времени</span>
                      </label>
                  </div>
              </div>
              <button class="btn btn--primary"><span>Перезвоните мне</span></button>
          </form>
          <div class="feedback-modal__footer">
              <p>Нажимая кнопку «Подать заявку» вы даёте своё <a href="">согласие на обработку персональных данных</a></p>
          </div>
      `);

      $(function() {
          $('.sidebar-services__list').accordion({
              active: false,
              collapsible: true,
              header: ".sidebar-services__tab"
          });

          $('#prices').accordion({
              active: 1,
              collapsible: true,
              header: ".prices__tab"
          });

          $('[data-faq]').accordion({
              active: false,
              collapsible: true,
              header: ".faq__tab"
          });

          if (matchMedia('(max-width: 1250px)').matches) {
              $('#plans').accordion({
                  active: false,
                  collapsible: true,
                  header: ".plan__header"
              });
          }

          if ($.fn.twentytwenty) {
              $("[data-work-slider]").twentytwenty({
                before_label: 'До', // Set a custom before label
                after_label: 'После' // Set a custom after label
              });

          }
        
          $('#reviews-tablist').tabs({ active: 0});

          $('[type="text"], [type="tel"], [type="email"]').on('change', function(e) {
              if (this.value) {
                 this.classList.add('has-value')
              } else {
                this.classList.remove('has-value')
              }
          })
          $('.field-wrapper__clear').on('click', function(e) {
              $(this).closest('.field-wrapper').find('[type="text"], [type="tel"], [type="email"]').removeClass('has-value')[0].value = ''
          })

      })


      ////////////////////////////////////////////////////////////


      ////////////////////////////////////////////////////////////

      ;(function() {
         var $sliders = $('[data-slider="big-slider"], [data-slider="video-slider"]');

         $sliders.each(function(i, slider) {
            
            var $slider = $(slider)
          

           var $big =  $slider.find('[data-slider-list="big"]');
           var $small = $slider.find('[data-slider-list="small"]');
           
           $big.slick({
              prevArrow: $slider.find('[data-slider-arrow-prev]'),
              nextArrow: $slider.find('[data-slider-arrow-next]'),
              infinite: true,
              slidesToShow: 1,
              slidesToScroll: 1,
              asNavFor:  $slider.find('[data-slider-list="small"]')
           });
           $small.slick({
              arrows: false,
              variableWidth: true,
              asNavFor:  $slider.find('[data-slider-list="big"]')
           });
           
           $small.on('click', '[data-item]', function(e) {
                e.preventDefault();
                $big.slick('slickGoTo', +e.currentTarget.dataset.item)
           })
         })

      })();

      $(".sidebar").stick_in_parent({
          offset_top: 10
      });



      ;(function() {
         var $sliders = $('[data-slider="mob-video-slider"]');

         $sliders.each(function(i, slider) {
            
          var $slider = $(slider)
          
           var $big =  $slider.find('[data-slider-list="big"]');
           var $small = $slider.find('[data-slider-list="small"]');
           
           $big.slick({
              
              prevArrow: $slider.find('[data-slider-arrow-prev]'),
              nextArrow: $slider.find('[data-slider-arrow-next]'),
              infinite: true,
              slidesToShow: 1,
              slidesToScroll: 1,
              asNavFor:  $slider.find('[data-slider-list="small"]'),
              mobileFirst: true,
               responsive: [
              {
                breakpoint: 500,
                settings: 'unslick',
              },
            ]
           });
           $small.slick({
              arrows: false,
              //infinite: true,
              //centerMode: true,
              variableWidth: true,
              //slidesToScroll: 6,
              //slidesToShow: 6,
              asNavFor:  $slider.find('[data-slider-list="big"]'),
              mobileFirst: true,
               responsive: [
              {
                breakpoint: 500,
                settings: 'unslick',
              },
            ]
           });
           
           $small.on('click', '[data-item]', function(e) {
                e.preventDefault();
                $big.slick('slickGoTo', +e.currentTarget.dataset.item)
           })
         })

      })();



      $('#advantages-list').slick({
            arrows: false,
            variableWidth: false,
            slidesToScroll: 1,
            slidesToShow: 1,
            mobileFirst: true,
            dots: true,
            responsive: [
              {
                breakpoint: 500,
                settings: 'unslick',
              },
            ]
      });


      



      ////////////////////////////////////////////////////////////

      var header = document.getElementById('header')
      var headerWrapper = document.getElementById('header-wrapper')
      var headerHeight = header.clientHeight;

      headerWrapper.style.height = headerHeight + 'px'

      window.addEventListener('scroll', function(e) {
         document.documentElement.classList[window.pageYOffset > headerHeight ? 'add' : 'remove']('header-fixed');
      })


      ///////////////////////////////////////////////////////////////
      $('[data-toggle-target]').each(function(i, target) {
           var $target = $(target)
           var $toggler = $('[data-toggle="'+target.id+'"]')
           var content = $target.find('[data-content]')[0];
           function toggleMobNav(s) {
              if (s) {
                  $target.removeClass('active')
                  $toggler.removeClass('active')
                  if ($target.hasClass('mob-nav-modal')) {
                      bodyScrollLock.enableBodyScroll(content)
                  }
              } else {
                  $target.addClass('active')
                  $toggler.addClass('active')
                  if ($target.hasClass('mob-nav-modal')) {
                      bodyScrollLock.disableBodyScroll(content)
                  }
              }
          }
         $toggler.on('click', function(e) {
             e.preventDefault()
             toggleMobNav($target.hasClass('active'))
         })
         $target.on('click', function(e) {
            if (!$(e.target).closest('[data-content]')[0]) {
                  toggleMobNav(1)
            }
          })

      })

      //$('[data-toggle="feedback-modal"]').trigger('click')

      //////////////////////////////////////////

       $("#licences, #certificates, #doctor-reviews").lightGallery({selector: 'a'}); 


});


$(function() {

   $('[data-slider="home-specials"]').slick({
            arrows: false,
            variableWidth: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            mobileFirst: true,
            dots: true,
            responsive: [
              {
                breakpoint: 500,
                settings: 'unslick',
              },
            ]
      });

   function checkMedia() {
       if (matchMedia('(max-width: 500px)').matches) {
            $('.home-advantages__content').tabs({ collapsible: true}).addClass('has-tabs');
       } else {
            $('.home-advantages__content.has-tabs').tabs( "destroy" ).removeClass('has-tabs');
       }
   }
   checkMedia()
   window.addEventListener('resize', checkMedia)


})